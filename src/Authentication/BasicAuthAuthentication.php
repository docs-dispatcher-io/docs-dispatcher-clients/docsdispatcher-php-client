<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Authentication;

use GuzzleHttp\ClientInterface as GuzzleHttpClientInterface;
use GuzzleHttp\RequestOptions;

class BasicAuthAuthentication implements AuthenticationInterface
{
    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    public function authenticate(GuzzleHttpClientInterface $guzzleClient, string &$endpointUrl, array &$options): void
    {
        $options[RequestOptions::AUTH] = [$this->login, $this->password];
    }
}
