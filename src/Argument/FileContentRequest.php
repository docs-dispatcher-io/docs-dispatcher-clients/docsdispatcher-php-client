<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

abstract class FileContentRequest extends BasicRequest
{
    /**
     * @var string|null
     */
    protected $content;

    /**
     * @var string|null
     */
    protected $url;

    public function __construct()
    {
    }

    public function buildPayload(): array
    {
        $payload = parent::buildPayload();

        if ($this->content) {
            $payload['content'] = $this->content;
        }

        if ($this->url) {
            $payload['url'] = $this->url;
        }

        return $payload;
    }

    public function setContent(?string $content = null): self
    {
        $this->content = $content;

        return $this;
    }

    public function setUrl(?string $url = null): self
    {
        $this->url = $url;

        return $this;
    }
}
