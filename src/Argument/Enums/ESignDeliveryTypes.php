<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class ESignDeliveryTypes
{
    public const EMAIL = 'EMAIL';
    public const URL = 'URL';
    public const SMS = 'SMS';
    public const WEB = 'WEB';
}
