<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class ColorModes
{
    public const COLOR = 'COLOR';
    public const BW = 'BW';
}
