<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class ESignFieldTypes
{
    public const SIGNATURE = 'SIGNATURE';
    public const VISA = 'VISA';
}
