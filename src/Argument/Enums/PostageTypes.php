<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument\Enums;

class PostageTypes
{
    public const ECO = 'ECO';
    public const REGISTERED_LETTER = 'REGISTERED_LETTER';
    public const REGISTERED_LETTER_WITH_ACK = 'REGISTERED_LETTER_WITH_ACK';
    public const FAST = 'FAST';
    public const FAST_TRACKED = 'FAST_TRACKED';
    public const SLOW = 'SLOW';
    public const SLOW_TRACKED = 'SLOW_TRACKED';
}
