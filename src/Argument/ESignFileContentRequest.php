<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

use DocsDispatcherIo\Sdk\Traits\MixedPropertyTrait;

class ESignFileContentRequest extends FileContentRequest
{
    use MixedPropertyTrait;

    /**
     * @var ESignField[]|null
     */
    protected $fields;

    public function setFields($field = null): self
    {
        $this->fields = $this->makeIterable($field);

        return $this;
    }

    public function addField(ESignField $field): self
    {
        if (!\is_array($this->fields)) {
            $this->fields = [];
        }

        $this->fields[] = $field;

        return $this;
    }

    public function buildPayload(): array
    {
        $payload = parent::buildPayload();

        if (\is_array($this->fields) && \count($this->fields) > 0) {
            $payload['fields'] = [];

            foreach ($this->fields as $field) {
                $payload['fields'][] = $field->buildPayload();
            }
        }

        return $payload;
    }
}
