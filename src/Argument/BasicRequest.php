<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Argument;

use DocsDispatcherIo\Sdk\RequestableInterface;
use DocsDispatcherIo\Sdk\Traits\WithDataTrait;
use DocsDispatcherIo\Sdk\Traits\WithTargetsTrait;

class BasicRequest implements RequestableInterface
{
    use WithDataTrait;
    use WithTargetsTrait;

    /**
     * @var string|null
     */
    protected $templateName;

    /**
     * @var string|null
     */
    protected $resultFileName;

    /**
     * @var array|null
     */
    protected $data;

    /**
     * @param AbstractTarget[]|null $targets
     */
    public function __construct(
        ?string $templateName = null,
        ?string $resultFileName = null,
        ?array $data = null,
        ?array $targets = null
    ) {
        $this->templateName = $templateName;
        $this->resultFileName = $resultFileName;
        $this->data = $data;
        $this->targets = $targets;
    }

    public function buildPayload(): array
    {
        if ($this->templateName) {
            $payload['templateName'] = $this->templateName;
        }

        if ($this->resultFileName) {
            $payload['resultFileName'] = $this->resultFileName;
        }

        $this->buildPayloadData($payload);

        return $this->buildPayloadTargets($payload);
    }

    public function setTemplateName(?string $templateName = null): self
    {
        $this->templateName = $templateName;

        return $this;
    }

    public function setResultFileName(?string $resultFileName = null): self
    {
        $this->resultFileName = $resultFileName;

        return $this;
    }
}
