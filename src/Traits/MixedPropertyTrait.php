<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Traits;

trait MixedPropertyTrait
{
    /**
     * @param array|string|null $input
     */
    private function makeIterable($input, bool $allowNull = true): ?array
    {
        if (null === $input) {
            return $allowNull ? $input : [];
        }

        return is_iterable($input) ? $input : [$input];
    }
}
