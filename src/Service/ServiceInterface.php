<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

use DocsDispatcherIo\Sdk\RequestableInterface;

interface ServiceInterface extends RequestableInterface
{
    /**
     * Return the remote service name to be used in endpoint(s).
     */
    public function getEndpointName(): string;
}
