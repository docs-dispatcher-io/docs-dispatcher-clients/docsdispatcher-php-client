<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

use DocsDispatcherIo\Sdk\Traits\MixedPropertyTrait;
use DocsDispatcherIo\Sdk\Traits\WithDataTrait;

class SmsService implements ServiceInterface
{
    use MixedPropertyTrait;
    use WithDataTrait;

    /**
     * @var array
     */
    protected $to;

    /**
     * @var string|null
     */
    protected $from;

    /**
     * @var string|null
     */
    protected $smsContent;

    /**
     * @var string|null
     */
    protected $templateName;

    /**
     * @var string|null
     */
    protected $provider;

    /**
     * @param array|string $to
     */
    public function __construct($to, ?string $smsContent = null)
    {
        $this->to = $this->makeIterable($to, false);
        $this->smsContent = $smsContent;
    }

    public function getEndpointName(): string
    {
        return 'sms';
    }

    public function buildPayload(): array
    {
        $payload = [
            'to' => $this->to,
        ];

        if ($this->from) {
            $payload['from'] = $this->from;
        }

        if ($this->smsContent) {
            $payload['smsContent'] = $this->smsContent;
        }

        if ($this->templateName) {
            $payload['templateName'] = $this->templateName;
        }

        if ($this->provider) {
            $payload['providerName'] = $this->provider;
        }

        return $this->buildPayloadData($payload);
    }

    public function setFrom(?string $from = null): self
    {
        $this->from = $from;

        return $this;
    }

    public function setSmsContent(?string $smsContent = null): self
    {
        $this->smsContent = $smsContent;

        return $this;
    }

    public function setTemplateName(?string $templateName = null): self
    {
        $this->templateName = $templateName;

        return $this;
    }

    public function addTo(string $to): self
    {
        if (!\is_array($this->to)) {
            $this->to = [];
        }

        $this->to[] = $to;

        return $this;
    }

    public function setProvider(?string $provider = null): self
    {
        $this->provider = $provider;

        return $this;
    }
}
