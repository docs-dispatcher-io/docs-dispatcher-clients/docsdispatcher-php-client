<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Service;

interface ComposableServiceInterface extends ServiceInterface
{
}
