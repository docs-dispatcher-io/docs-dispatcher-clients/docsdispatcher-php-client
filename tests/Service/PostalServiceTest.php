<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Argument\Address;
use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Argument\Enums\ColorModes;
use DocsDispatcherIo\Sdk\Argument\Enums\EnvelopeFormats;
use DocsDispatcherIo\Sdk\Argument\Enums\PostageTypes;
use DocsDispatcherIo\Sdk\Service\PostalService;
use DocsDispatcherIo\Sdk\Tests\Utils\WithTargets;
use PHPUnit\Framework\TestCase;

class PostalServiceTest extends TestCase
{
    use WithTargets;

    public function testGetEndpointName()
    {
        $this->assertEquals('postal', (new PostalService(
            $this->getAddress(),
            $this->getReceiver(),
            $this->getDocument()
        ))->getEndpointName());
    }

    private function getAddress(string $addressName = 'address name'): Address
    {
        return new Address($addressName, 'address1', 'city', '02341', 'FR');
    }

    private function getReceiver(): Address
    {
        return $this->getAddress('recipient');
    }


    private function getDocument(): Attachment
    {
        return (new Attachment())
            ->setTemplateName($this->templateName);
    }

    private function getFullDocument(): Attachment
    {
        return (new Attachment())
            ->setTemplateName($this->templateName)
            ->setResultFileName('output.pdf')
            ->setData(['some' => ['super' => 'content']])
            ->setTargets([$this->getTarget1()])
            ->addTarget($this->getTarget2());
    }

    protected $templateName = 'template-name';

    public function testSimplePayloadWithFixedMessages()
    {
        $service = (new PostalService(
            $this->getAddress(),
            $this->getReceiver(),
            $this->getDocument()
        ))
            ->setProvider('provider')
            ->setTargets($this->getTargets());

        $payload = $service->buildPayload();
        $this->assertEquals($this->getAddress()->buildPayload(), $payload['sender']);
        $this->assertEquals('provider', $payload['provider']);
        $this->assertCount(1, $payload['receivers']);
        $this->assertEquals('recipient', $payload['receivers'][0]['name']);
        $this->assertCount(1, $payload['documents']);
        $this->assertEquals($this->templateName, $payload['documents'][0]['templateName']);
        $this->assertCount(2, $payload['targets']);
    }

    protected $settings = ['some' => 'settings'];

    public function testComplexPayload()
    {
        $service = (new PostalService(
            $this->getAddress(),
            $this->getReceiver(),
            $this->getDocument()
        ))
            ->addReceiver($this->getAddress('receiver 2'))
            ->addDocument($this->getFullDocument())
            ->setProvider('provider')
            ->setTargets($this->getTargets())
            ->setEnvelopeFormat(EnvelopeFormats::AUTO)
            ->setColorMode(ColorModes::BW)
            ->setPostage(PostageTypes::ECO)
            ->setBothSides(true)
            ->setTargetFilename('output.pdf')
            ->setSettings($this->settings)
            ->addSetting('key', 'value');

        $payload = $service->buildPayload();

        $this->assertEquals($this->getAddress()->buildPayload(), $payload['sender']);
        $this->assertEquals($this->getTargetsPayload(), $payload['targets']);
        $this->assertEquals('receiver 2', $payload['receivers'][1]['name']);
        $this->assertContains($this->getReceiver()->buildPayload(), $payload['receivers']);
        $this->assertEquals('provider', $payload['provider']);

        $this->assertEquals(EnvelopeFormats::AUTO, $payload['envelopeFormat']);
        $this->assertEquals(ColorModes::BW, $payload['colorMode']);
        $this->assertEquals(PostageTypes::ECO, $payload['postage']);
        $this->assertTrue($payload['bothSides']);
        $this->assertEquals('output.pdf', $payload['targetFilename']);
        $this->assertEquals(array_merge($this->settings, ['key' => 'value']), $payload['settings']);
    }
}
