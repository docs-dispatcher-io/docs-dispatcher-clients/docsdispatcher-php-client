<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Service;

use DocsDispatcherIo\Sdk\Service\SmsService;
use PHPUnit\Framework\TestCase;

class SmsServiceTest extends TestCase
{
    private const TO_NUMBER = '+33123456789';
    private const TO_NUMBER2 = '+33123456790';

    public function testGetEndpointName()
    {
        $this->assertEquals('sms', (new SmsService(null))->getEndpointName());
    }

    public function testBuildPayload()
    {
        $smsService = (new SmsService(self::TO_NUMBER, 'Some content'))
            ->setData(['some' => ['cool' => 'data']])
            ->setFrom('911')
            ->setProvider('messaging-corp')
            ->setTemplateName('my-template');

        $payload = $smsService->buildPayload();

        $this->assertContains(self::TO_NUMBER, $payload['to']);
        $this->assertEquals('Some content', $payload['smsContent']);
        $this->assertEquals(['some' => ['cool' => 'data']], $payload['data']);
        $this->assertEquals('911', $payload['from']);
        $this->assertEquals('messaging-corp', $payload['providerName']);
        $this->assertEquals('my-template', $payload['templateName']);
    }

    public function testAnotherPayload()
    {
        $smsService = (new SmsService(self::TO_NUMBER, 'Some old content'));
        $payload = $smsService
            ->setSmsContent('Some content')
            ->addTo(self::TO_NUMBER2)
            ->buildPayload();

        $this->assertContains(self::TO_NUMBER, $payload['to']);
        $this->assertContains(self::TO_NUMBER2, $payload['to']);
        $this->assertEquals('Some content', $payload['smsContent']);
    }
}
