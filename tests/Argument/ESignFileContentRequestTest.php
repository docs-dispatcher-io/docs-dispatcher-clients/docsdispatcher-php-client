<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Argument;

use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Argument\Enums\ESignFieldTypes;
use DocsDispatcherIo\Sdk\Argument\ESignField;
use DocsDispatcherIo\Sdk\Argument\ESignFileContentRequest;
use DocsDispatcherIo\Sdk\Tests\Utils\WithTargets;
use PHPUnit\Framework\TestCase;

class ESignFileContentRequestTest extends TestCase
{
    use WithTargets;

    protected function getESignField(): ESignField
    {
        return (new ESignField('recipient@email.com', 'signer.email'))
            ->setType(ESignFieldTypes::SIGNATURE)
            ->setConsents(['consent1'])
            ->addConsent('consent2');
    }

    protected function getESignFieldPayload(): array
    {
        return [
            'recipient' => 'recipient@email.com',
            'anchorKey' => 'signer.email',
            'type' => 'SIGNATURE',
            'consents' => ['consent1', 'consent2']
        ];
    }

    public function testBuildPayload()
    {
        $esignFile = (new ESignFileContentRequest())
            ->setTemplateName('template-name')
            ->setResultFileName('output.pdf')
            ->setData(['some' => ['super' => 'content']])
            ->setTargets([$this->getTarget1()])
            ->addTarget($this->getTarget2())
            ->addField($this->getESignField());

        $payload = $esignFile->buildPayload();

        $this->assertEquals('template-name', $payload['templateName']);
        $this->assertEquals('output.pdf', $payload['resultFileName']);
        $this->assertEquals(['some' => ['super' => 'content']], $payload['data']);
        $this->assertEquals(
            [$this->getPayload1(), $this->getPayload2()],
            $payload['targets']
        );
        $this->assertEquals(
            [$this->getESignFieldPayload()],
            $payload['fields']
        );
    }
}
