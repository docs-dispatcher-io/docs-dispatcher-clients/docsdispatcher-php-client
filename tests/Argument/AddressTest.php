<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Argument;

use DocsDispatcherIo\Sdk\Argument\Address;
use DocsDispatcherIo\Sdk\Argument\Enums\ESignRecipientRoles;
use DocsDispatcherIo\Sdk\Argument\Recipient;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    protected $name = 'name';
    protected $address1 = 'address1';
    protected $address2 = 'address2';
    protected $address3 = 'address3';
    protected $address4 = 'address4';
    protected $city = 'city';
    protected $zipCode = 'zipCode';
    protected $countryCode = 'countryCode';

    public function testBuildPayload()
    {
        $address = (new Address(
            $this->name,
            $this->address1,
            $this->city,
            $this->zipCode,
            $this->countryCode
        ))
            ->setAddress2($this->address2)
            ->setAddress3($this->address3)
            ->setAddress4($this->address4);
        $payload = $address->buildPayload();

        $this->assertEquals($this->name, $payload['name']);
        $this->assertEquals($this->address1, $payload['address1']);
        $this->assertEquals($this->address2, $payload['address2']);
        $this->assertEquals($this->address3, $payload['address3']);
        $this->assertEquals($this->address4, $payload['address4']);
        $this->assertEquals($this->city, $payload['city']);
        $this->assertEquals($this->zipCode, $payload['zipCode']);
        $this->assertEquals($this->countryCode, $payload['countryCode']);
    }
}
