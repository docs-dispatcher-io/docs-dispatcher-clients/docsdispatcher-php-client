<?php

declare(strict_types=1);

namespace DocsDispatcherIo\Sdk\Tests\Argument;

use DocsDispatcherIo\Sdk\Argument\Attachment;
use DocsDispatcherIo\Sdk\Tests\Utils\WithTargets;
use PHPUnit\Framework\TestCase;

class AttachmentTest extends TestCase
{
    use WithTargets;

    public function testBuildPayload()
    {
        $attachement = (new Attachment())->setTemplateName('template-name')
            ->setResultFileName('output.pdf')
            ->setData(['some' => ['super' => 'content']])
            ->setTargets([$this->getTarget1()])
            ->addTarget($this->getTarget2());


        $payload = $attachement->buildPayload();

        $this->assertEquals('template-name', $payload['templateName']);
        $this->assertEquals('output.pdf', $payload['resultFileName']);
        $this->assertEquals(['some' => ['super' => 'content']], $payload['data']);
        $this->assertEquals(
            [$this->getPayload1(), $this->getPayload2()],
            $payload['targets']
        );
    }

    public function testURLContent()
    {
        $payload = (new Attachment())
            ->setResultFileName('output.pdf')
            ->setData(['some' => ['super' => 'content']])
            ->setUrl('http://example.com')
            ->buildPayload();
        $this->assertEquals('output.pdf', $payload['resultFileName']);
        $this->assertEquals('http://example.com', $payload['url']);
    }

    public function testTextContent()
    {
        $payload = (new Attachment())
            ->setResultFileName('output.pdf')
            ->setData(['some' => ['super' => 'content']])
            ->setContent('{{url}} template')
            ->buildPayload();
        $this->assertEquals('output.pdf', $payload['resultFileName']);
        $this->assertEquals('{{url}} template', $payload['content']);
    }
}
